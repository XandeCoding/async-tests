import aiohttp
import asyncio
import socket
from fastapi import FastAPI
from pydantic import BaseModel
from typing import Optional, Dict, List

app = FastAPI()

conn = aiohttp.TCPConnector(
        family=socket.AF_INET,
        ssl=False,
    )

@app.on_event('startup')
async def startup_event():
    global session
    session = aiohttp.ClientSession(connector=conn)

@app.on_event('shutdown')
async def shutdown_event():
    await session.close()

class ChuckJoke(BaseModel):
    id: int
    joke: str
    categories: Optional[List[str]] = None

async def getChuckJokes(quantity: int):
    async with session.get(f'http://api.icndb.com/jokes/random/{quantity}') as response:
        jokes = await response.json()

        if (jokes['type'] != 'success'):
            return null
           
        return [ChuckJoke.parse_obj(joke) for joke in jokes['value']]
            

async def getSooMuchJokes():
    tasks = []

    for page in range(1, 3):
        task = asyncio.create_task(getChuckJokes(page*2))
        tasks.append(task)

        unflattedJokes = await asyncio.gather(*tasks)
        return sum(unflattedJokes, [])

@app.get('/', response_model=List[ChuckJoke])
async def root():
    return await getSooMuchJokes()


